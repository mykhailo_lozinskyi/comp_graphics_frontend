export const complex_mul = ([a, b], [c, d]) => [a*c - b*d, a*d + b*c]
export const complex_sub = ([a, b], [c, d]) => [a-c, b-d]
export const complex_add = ([a, b], [c, d]) => [a+c, b+d]
export const complex_sqdist = ([a, b], [c, d]) => (a-c)**2 + (b-d)**2

export const complex_div = ([a, b], [c, d]) => {
  let den = c**2 + d**2
  return [(a*c+b*d)/den, (b*c-a*d)/den]
}

export const complex_pow = ([a, b], [c, d]) => {
  let r = Math.sqrt(a**2 + b**2)
  let theta = Math.atan2(b, a)
  let x = r**c * Math.exp(-d * theta)
  let y = c * theta + d * Math.log(r)
  return [x * Math.cos(y), x * Math.sin(y)]
}
export function newtonsMethod(f, fp, x0, steps) {
  let xn = x0;
  for (let i = 0; i < steps; i++) {
    xn = complex_sub(xn, complex_div(f(xn), fp(xn)))
  }
  return xn
}