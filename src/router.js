import { createRouter, createWebHistory } from 'vue-router';
import MainPage from './components/MainPage.vue';
import Lab01Page from './components/Lab01Page.vue';
import Lab02Page from './components/Lab02Page.vue';
import Lab03Page from './components/Lab03Page.vue';
import MaterialsPage from './components/MaterialsPage.vue';
import TestsPage from './components/TestsPage.vue';
import HelpPage from './components/HelpPage.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: MainPage, 
      name: 'main',
    },
    {
      path: '/Lab01Page',
      component: Lab01Page,
      name: 'lab-work-01',
    },
    {
      path: '/Lab02Page',
      component: Lab02Page, 
      name: 'lab-work-02', 
    },
    {
      path: '/Lab03Page',
      component: Lab03Page, 
      name: 'lab-work-03',
    },
    {
      path: '/Materials',
      component: MaterialsPage, 
      name: 'materials', 
    },
    {
      path: '/Help',
      component: HelpPage, 
      name: 'help', 
    },
    {
      path: '/Tests',
      component: TestsPage, 
      name: 'tests', 
    },
  ]
});

export default router;
