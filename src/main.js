import { createApp } from 'vue';
import App from './App.vue';
import router from './router'; // Import the router you defined

createApp(App).use(router).mount('#app');
